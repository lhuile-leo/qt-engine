/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** Property
*/

#pragma once

#include "ClassType.hpp"
#include <QtCore/QMetaMethod>
#include <QtCore/QMetaType>
#include <QtCore/QString>
#include <QtCore/QUuid>

namespace types {
	class Property : public ClassType {
		Q_OBJECT

	public:
		Property();
		Property(const QMetaProperty &metaProperty);
		~Property() = default;

		QJsonObject serialize() const override;
		void deserialize(const QJsonObject &json) override;

		QWidget *initEditor() override;

		bool isValid() const override { return !_name.isEmpty(); }
		QString signature() const override { return _type + " " + _name; }

		bool isUserType() const { return _userType; }

		QString type() const { return _type; }
		void setType(const QString &type) { setValue(_type, type, std::bind(&Property::typeChanged, this, _type)); }

		QString name() const { return _name; }
		void setName(const QString &name) { setValue(_name, name, std::bind(&Property::nameChanged, this, _name)); }

		QString setterSignature() const;
		QString setterName() const { return _setterName; }
		void setSetterName(const QString &setterName) { setValue(_setterName, setterName, std::bind(&Property::setterNameChanged, this, _setterName)); }

		QString getterSignature() const;
		QString getterName() const { return _getterName; }
		void setGetterName(const QString &getterName) { setValue(_getterName, getterName, std::bind(&Property::getterNameChanged, this, _getterName)); }

		QUuid id() const { return _id; }

	signals:
		void typeChanged(const QString &type);
		void nameChanged(const QString &name);
		void setterNameChanged(const QString &setterName);
		void getterNameChanged(const QString &getterName);

	private:
		bool _userType;
		QString _type;
		QString _name;
		QString _setterName;
		QString _getterName;
		QUuid _id;
	};
}

QDebug operator<<(QDebug debug, const types::Property &property);
QDebug operator<<(QDebug debug, const types::Property *property);
