/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** EProgressBar
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QProgressBar>

namespace libraryObjects {
	typedef Object<QProgressBar> EProgressBar;
}
