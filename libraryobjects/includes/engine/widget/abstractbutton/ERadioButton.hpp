/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** ERadioButton
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QRadioButton>

namespace libraryObjects {
	typedef Object<QRadioButton> ERadioButton;
}
