/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** EAction
*/

#pragma once

#include "Object.hpp"
#include <QtWidgets/QAction>

namespace libraryObjects {
	typedef Object<QAction> EAction;
}
