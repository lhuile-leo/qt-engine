/*
** EPITECH PROJECT, 2020
** qt-engine
** File description:
** ObjectManager
*/

#pragma once

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QUuid>

namespace libraryObjects {
	class AObject;

	class ObjectManager : public QObject {
	public:
		~ObjectManager() = default;
		static ObjectManager *instance();

		bool registerObject(AObject *object);
		void unregisterObject(AObject *object);

		void registerCustomObject(const QUuid &objectId, const QString &objectClassName, const QString &objectName);
		void unregisterCustomObject(const QUuid &objectId);

		void setObjectAsRoot(AObject *rootObject, const QString &rootObjectClassName) { _rootObject = rootObject; _rootObjectClassName = rootObjectClassName; }

		AObject *object(const QString &objectName) const { return _objects.contains(objectName) ? _objects[objectName] : nullptr; }
		QString objectName(AObject *object) const { return _objects.key(object); }

		AObject *object(const QUuid &objectId) const { return _objectsId.contains(objectId) ? _objectsId[objectId] : nullptr; }
		QString objectClassName(const QUuid &objectId) const;
		QString objectName(const QUuid &objectId) const;

	private slots:
		void onPropertyUpdated(const QString &propertyName, const QVariant &propertyValue);

	private:
		ObjectManager() = default;
		QMap<QString, AObject*> _objects;
		QMap<QUuid, AObject *> _objectsId;
		QMap<QUuid, QPair<QString, QString>> _customObjectsId;
		AObject *_rootObject = nullptr;
		QString _rootObjectClassName;
	};
}
